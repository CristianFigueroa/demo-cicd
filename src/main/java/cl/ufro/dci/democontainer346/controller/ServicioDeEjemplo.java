package cl.ufro.dci.democontainer346.controller;


import cl.ufro.dci.democontainer346.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ServicioDeEjemplo {

    private static List<User> listaUsuario = new ArrayList<>();

    /**
     * ejemplo para javadoc
     */
    @GetMapping("/users")
    public List<User> getUsers(){
        this.listaUsuario.add(new User("Juan", "Perez"));
        this.listaUsuario.add(new User("Maria", "Lopez"));
        return this.listaUsuario;
    }
}
