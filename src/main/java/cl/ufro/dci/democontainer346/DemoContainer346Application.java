package cl.ufro.dci.democontainer346;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoContainer346Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoContainer346Application.class, args);
	}

}
