package cl.ufro.dci.democontainer346.model;

public class User {

    public String name;
    public String userName;

    public User(String name, String userName) {
        this.name = name;
        this.userName = userName;
    }
}
