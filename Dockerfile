FROM openjdk:8-jdk-alpine

VOLUME /tmp

EXPOSE 8080

COPY target/demo-container346-0.0.1-SNAPSHOT.jar demo-container346.jar

ENTRYPOINT ["java","-jar","demo-container346.jar"]